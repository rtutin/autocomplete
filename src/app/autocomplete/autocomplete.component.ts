import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent {
  open: boolean = false;
  more: boolean = false;
  allowToggle: boolean = true;
  items: Array<string> = ['Lorem ipsum', 'dolor sit amet', 'consectetur adipisicing elit', 'Vel harum repudiandae', 'doloribus eaque', 'similique', 'dolore', 'in quia ratione', 'illo quam voluptatem', 'natus unde distinctio', 'dolorum maiores blanditiis', 'Commodi', 'adipisci', 'debitis'];
  searchList: Array<any> = [];
  query: string = '';
  selectedItems: Array<string> = [];

  constructor() {}

  toggle() {
    if (this.allowToggle) {
      this.open = !this.open;
    }

    this.allowToggle = true;
  }

  remove() {
    this.selectedItems = [];
    this.allowToggle = false;
  }

  getItemsForView() {
    if (this.more) {
      return this.selectedItems;
    } else {
      return this.selectedItems.slice(0, 10);
    }
  }

  select(e) {
    if (e.target.checked) {
      this.selectedItems.push(e.target.value);
    } else {
      if (this.selectedItems.indexOf(e.target.value) != -1) {
        this.selectedItems.splice(this.selectedItems.indexOf(e.target.value), 1);
      }
    }
  }

  toggleMore() {
    this.more = !this.more;
  }

  change(e) {
    let result: Array<any> = [];
    this.query = e.target.value;

    for (let item of this.items) {
      if (item.indexOf(this.query) != -1) {
        result.push({
          checked: this.selectedItems.indexOf(item) != -1,
          value: item
        });
      }
    }

    this.searchList = result;
  }

  removeItem(i: number) {
    this.selectedItems.splice(i, 1);
  }
}
